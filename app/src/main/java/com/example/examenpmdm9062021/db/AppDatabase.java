package com.example.examenpmdm9062021.db;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.examenpmdm9062021.db.model.Socio;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Socio.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {


    public abstract SocioDao socioDao();

    public static AppDatabase INSTANCE;
    private static final int THREADS = 6;

    public static final ExecutorService executorService = Executors.newFixedThreadPool(THREADS);

    public static AppDatabase getDBinstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "sociosdb")
                    //.allowMainThreadQueries()
                    .build();
        }

        return INSTANCE;
    }

    private static List<Socio> socioList = new ArrayList<>();

    public static void addPetToList(Socio socio){
        socioList.add(socio);
    }


    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDBAsync(INSTANCE).execute();
        }
    };

    private static class PopulateDBAsync extends AsyncTask<Void, Void, Void> {

        Socio socio;
        private SocioDao socioDao;

        private PopulateDBAsync(AppDatabase db) {
            socioDao = db.socioDao();
        }


        @Override
        protected Void doInBackground(Void... voids) {

            for(int i = 0; i < socioList.size(); i++){
                socioDao.insertSocio(socioList.get(i));
            }

            return null;
        }
    }

}

