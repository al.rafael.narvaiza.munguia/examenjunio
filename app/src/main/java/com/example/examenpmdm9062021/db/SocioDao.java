package com.example.examenpmdm9062021.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.example.examenpmdm9062021.db.model.Socio;

import java.util.List;

@Dao
public interface SocioDao {

    @Query("SELECT * FROM Socio")
    List<Socio> getAllSocios();

    @Query("UPDATE Socio SET nombreCompleto = :nombreCompleto, direccionCalle = :direccionCalle, direccionProvincia = :direccionProvincia, fecha = :fecha, imagen = :imagen WHERE id = :id")
    void updateSelectedSocio(String nombreCompleto, String direccionCalle, String direccionProvincia, String fecha, String imagen, int id);



    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertSocio(Socio socio);

    @Transaction
    @Query("SELECT * FROM Socio WHERE id = :id")
    LiveData<Socio> getUniqueSocio(int id);

    @Delete
    void deleteSocio(Socio socio);

    @Update(entity = Socio.class)
    void updateSocio(Socio socio);


}
