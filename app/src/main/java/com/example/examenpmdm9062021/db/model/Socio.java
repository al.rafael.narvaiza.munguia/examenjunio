package com.example.examenpmdm9062021.db.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

@Entity
public class Socio {
    @NonNull
    @ColumnInfo(name="id")
    @PrimaryKey
    private int id;

    @NonNull
    @ColumnInfo(name="nombreCompleto")
    private String nombreCompleto;

    @NonNull
    @ColumnInfo(name="direccionCalle")
    private String direccionCalle;

    @NonNull
    @ColumnInfo(name="direccionProvincia")
    private String direccionProvincia;

    @NonNull
    @ColumnInfo(name="fecha")
    private String fecha;

    @NonNull
    @ColumnInfo(name="imagen")
    private String imagen;

    public Socio(int id, String nombreCompleto, String direccionCalle, String direccionProvincia, String fecha, String imagen) {
        this.setId(id);
        this.setNombreCompleto(nombreCompleto);
        this.setDireccionCalle(direccionCalle);
        this.setDireccionProvincia(direccionProvincia);
        this.setFecha(fecha);
        this.setImagen(imagen);
    }

    public Socio(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getDireccionCalle() {
        return direccionCalle;
    }

    public void setDireccionCalle(String direccionCalle) {
        this.direccionCalle = direccionCalle;
    }

    public String getDireccionProvincia() {
        return direccionProvincia;
    }

    public void setDireccionProvincia(String direccionProvincia) {
        this.direccionProvincia = direccionProvincia;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @NonNull
    @NotNull
    @Override
    public String toString() {
        return "Socio{" +
                "nombre='" + nombreCompleto + '\'' +
                ", direccion='" + direccionCalle + '\'' +
                ", provincia='" + direccionProvincia + '\'' +
                ", fecha='" + fecha + '\'' +
                ", url de imagen='" + imagen + '\'' +
                '}';
    }
}
