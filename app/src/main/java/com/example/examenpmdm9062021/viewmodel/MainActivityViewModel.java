package com.example.examenpmdm9062021.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.examenpmdm9062021.db.model.Socio;
import com.example.examenpmdm9062021.repo.Repository;

import java.util.ArrayList;
import java.util.List;

public class MainActivityViewModel extends AndroidViewModel {

    private MutableLiveData<List<Socio>> listOfSocios;
    private final Repository repository;


    public MainActivityViewModel(@NonNull Application application){
        super(application);
        repository = new Repository(application);
        listOfSocios = new MutableLiveData<>();
    }

    public MutableLiveData<List<Socio>> getListOfSociosObserver(){
        return listOfSocios;
    }


    public void getAllSociosList(){
        List<Socio> petList = repository.getAllSocioList();
        if(petList.size() > 0){
            listOfSocios.postValue(petList);
        }
        else {
            listOfSocios.postValue(null);
        }
    }

    public void setAllPetList(Socio socio){
        List<Socio> socioList = new ArrayList<>();
        socioList.add(socio);
    }

    public void insertSocio(int id, String nombreCompleto, String direccionCalle, String direccionProvincia, String fecha, String imagen){
        Socio socio = new Socio();
        socio.setId(id);
        socio.setNombreCompleto(nombreCompleto);
        socio.setDireccionCalle(direccionCalle);
        socio.setDireccionProvincia(direccionProvincia);
        socio.setFecha(fecha);
        socio.setImagen(imagen);

        repository.insert(socio);
        getAllSociosList();
    }

    public void updateSelectedSocio(Socio socio){
        repository.update(socio);
        getAllSociosList();
    }

    public void deleteSocio(Socio socio){
        repository.delete(socio);
        getAllSociosList();
    }

}
