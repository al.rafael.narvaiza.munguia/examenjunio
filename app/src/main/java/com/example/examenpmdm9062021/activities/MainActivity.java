package com.example.examenpmdm9062021.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.examenpmdm9062021.databinding.ActivityMainBinding;
import com.example.examenpmdm9062021.db.model.Socio;
import com.example.examenpmdm9062021.recycler.SocioListAdapter;
import com.example.examenpmdm9062021.service.DownloadService;
import com.example.examenpmdm9062021.viewmodel.MainActivityViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity implements SocioListAdapter.HandlePetClick{

    public static final String ACTION_RESP = "RESPUESTA_DESCARGA";
    private MainActivityViewModel viewModel;
    SocioListAdapter socioListAdapter;
    ActivityMainBinding binding;
    Intent intent;
    IntentFilter intentFilter;
    BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        intentFilter = new IntentFilter(ACTION_RESP);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastReceiver = new receptorOperacion();

        intent = new Intent(MainActivity.this, DownloadService.class);
        startService(intent);

        /*
        ViewModelProvider.AndroidViewModelFactory factory = ViewModelProvider.AndroidViewModelFactory.getInstance(getApplication());
        viewModel = new ViewModelProvider(this, factory).get(MainActivityViewModel.class);

         */
        binding.addNewSocioImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddSocioActivity.class);
                startActivity(intent);
            }
        });
        initViewModel();
        initRecyclerView();
        viewModel.getAllSociosList();


    }
    @Override
    public void onResume(){
        super.onResume();
        registerReceiver(broadcastReceiver, intentFilter);
        viewModel.getAllSociosList();
    }


    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        showMessage("Servicio parado");
        stopService(intent);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        registerReceiver(broadcastReceiver, intentFilter);
        binding.recyclerView.setVisibility(View.VISIBLE);
        viewModel.getAllSociosList();
    }
    private void initRecyclerView() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        socioListAdapter = new SocioListAdapter(this, this);
        binding.recyclerView.setAdapter(socioListAdapter);
    }

    private void initViewModel() {
        viewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);
        viewModel.getListOfSociosObserver().observe(this, new Observer<List<Socio>>() {
            @Override
            public void onChanged(List<Socio> socios) {
                if (socios == null){
                    binding.emptyListTextView.setVisibility(View.VISIBLE);
                    binding.recyclerView.setVisibility(View.GONE);
                }else{
                    socioListAdapter.setSocioList(socios);
                    binding.recyclerView.setVisibility(View.VISIBLE);
                    binding.emptyListTextView.setVisibility(View.GONE);
                }
            }
        });
    }



    private class receptorOperacion extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String respuesta = intent.getStringExtra("resultado");
            showMessage("Socios añadidas");
            cargarDB(respuesta);
        }
    }


    private void cargarDB(String respuesta) {
        String[] lineas = respuesta.split("\n");
        int id;
        String nombreCompleto, direccionCalle, direccionProvincia, fecha, imagen;
        for(int i =0; i< lineas.length; i++){
            String[] socios = lineas[i].split(";");
            id= Integer.valueOf(socios[0].trim());
            nombreCompleto=socios[1].trim();
            direccionCalle=socios[2].trim();
            direccionProvincia=socios[3].trim();
            fecha=socios[4].trim();
            imagen="https://dam.org.es/" + socios[5].trim();
            Socio socio = new Socio(id, nombreCompleto, direccionCalle, direccionProvincia, fecha, imagen);
            viewModel.insertSocio(id, nombreCompleto, direccionCalle,direccionProvincia, fecha, imagen);
            viewModel.getAllSociosList();


        }
    }

    @Override
    public void editSocio(Socio socio){
        viewModel.updateSelectedSocio(socio);
    }

    @Override
    public void deleteSocio(Socio socio){
        viewModel.deleteSocio(socio);
    }

    private void showMessage(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}