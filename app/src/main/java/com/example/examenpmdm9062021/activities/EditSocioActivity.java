package com.example.examenpmdm9062021.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.example.examenpmdm9062021.databinding.ActivityEditSocioBinding;
import com.example.examenpmdm9062021.db.model.Socio;
import com.example.examenpmdm9062021.sandbox.Utils;
import com.example.examenpmdm9062021.viewmodel.MainActivityViewModel;

import java.util.List;

public class EditSocioActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private ActivityEditSocioBinding binding;
    private MainActivityViewModel viewModelUpdate;
    private Socio updatedSocio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEditSocioBinding.inflate(getLayoutInflater());
        View v = binding.getRoot();
        setContentView(v);
        initViewModel();
        getIncomingIntent();
        binding.buttonDiscard.setOnClickListener(v1->finish());
        binding.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id;
                String nombreCompleto, direccionCalle, direccionProvincia, fecha, imagen;

                id = Integer.valueOf(binding.editTextTextIdSocio.getText().toString());
                nombreCompleto = binding.editTextTextSocioName.getText().toString();
                direccionCalle = binding.editTextTextDireccionCalle.getText().toString();
                direccionProvincia = binding.editTextDireccionProvincia.getText().toString();
                fecha = binding.editTextTextFechaSocio.getText().toString();
                imagen = binding.editTextTextSocioImagen.getText().toString();
                if(!Utils.inputFieldsAreChecked(id, nombreCompleto, direccionCalle, direccionProvincia, imagen, fecha).isEmpty()){
                    showMessage(Utils.inputFieldsAreChecked(id, nombreCompleto, direccionCalle, direccionProvincia, imagen, fecha));
                }
                else{
                    updatedSocio = new Socio(id, nombreCompleto, direccionCalle, direccionProvincia, fecha, imagen);
                    //TODO añadir un comprobante de que ese id todavía no está en la base de datos.
                    viewModelUpdate.updateSelectedSocio(updatedSocio);
                    finish();
                }
            }
        });
    }

    private void getIncomingIntent() {
        int id;
        String nombreCompleto, direccionCalle, direccionProvincia, fecha, imagen;
        if(
                getIntent().hasExtra("id")
                        && getIntent().hasExtra("nombre")
                        && getIntent().hasExtra("direccionCalle")
                        && getIntent().hasExtra("imagen")
                        && getIntent().hasExtra("direccionProvincia")
                        && getIntent().hasExtra("fecha")
        ){
            id = getIntent().getIntExtra("id", 0);
            nombreCompleto = getIntent().getStringExtra("nombre");
            direccionCalle = getIntent().getStringExtra("direccionCalle");
            imagen = getIntent().getStringExtra("imagen");
            direccionProvincia = getIntent().getStringExtra("direccionProvincia");
            fecha = getIntent().getStringExtra("fecha");

            binding.textViewEditSocio.setText("Socio con id" + id);
            binding.editTextTextIdSocio.setText(String.valueOf(id));
            binding.editTextTextSocioName.setText(nombreCompleto);
            binding.editTextTextDireccionCalle.setText(direccionCalle);
            binding.editTextDireccionProvincia.setText(direccionProvincia);
            binding.editTextTextSocioImagen.setText(imagen);
            binding.editTextTextFechaSocio.setText(fecha);


        }
    }

    private void initViewModel() {
        viewModelUpdate = new ViewModelProvider(this).get(MainActivityViewModel.class);
        viewModelUpdate.getListOfSociosObserver().observe(this, new Observer<List<Socio>>() {
            @Override
            public void onChanged(List<Socio> socios) {

            }
        });
    }


    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        parent.getItemAtPosition(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}