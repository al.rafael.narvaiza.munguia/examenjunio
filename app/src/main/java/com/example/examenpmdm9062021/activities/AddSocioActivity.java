package com.example.examenpmdm9062021.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.example.examenpmdm9062021.databinding.ActivityAddSocioBinding;
import com.example.examenpmdm9062021.sandbox.Utils;
import com.example.examenpmdm9062021.viewmodel.MainActivityViewModel;

public class AddSocioActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private ActivityAddSocioBinding binding;
    private MainActivityViewModel viewModelInsert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAddSocioBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        viewModelInsert = new ViewModelProvider(this).get(MainActivityViewModel.class);
        binding.buttonDiscard.setOnClickListener(v->finish());
        binding.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id;
                String nombreCompleto, direccionCalle, direccionProvincia, fecha, imagen;
                nombreCompleto = binding.editTextTextSocioName.getText().toString();
                direccionCalle = binding.editTextTextDireccionCalle.getText().toString();
                direccionProvincia = binding.editTextDireccionProvincia.getText().toString();
                fecha = binding.editTextTextFechaSocio.getText().toString();
                imagen = binding.editTextTextSocioImagen.getText().toString();
                id = Integer.valueOf(binding.editTextTextIdSocio.getText().toString());
                if(!Utils.inputFieldsAreChecked(id, nombreCompleto, direccionCalle, direccionProvincia, imagen, fecha).isEmpty()){
                    showMessage(Utils.inputFieldsAreChecked(id, nombreCompleto, direccionCalle, direccionProvincia, imagen, fecha));
                }
                else{
                    //TODO añadir un comprobante de que ese id todavía no está en la base de datos.
                    viewModelInsert.insertSocio(id, nombreCompleto, direccionCalle, direccionProvincia, fecha, imagen);
                    finish();
                }
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        parent.getItemAtPosition(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}