package com.example.examenpmdm9062021.sandbox;

import android.text.TextUtils;

public class Utils {


    public static String inputFieldsAreChecked(int id, String nombre, String direccionCalle, String direccionProvincia, String imagen, String fecha ) {
        String mensaje = "";
        boolean error = false;
        if (TextUtils.isEmpty(nombre)) {
            mensaje = mensaje + "\nEs necesario introducir un nombre para el socio.";
            error = true;
        }
        if (TextUtils.isEmpty(direccionCalle)) {
            mensaje = mensaje + "\nEs necesario introducir una direccion de la calle del socio.";
            error = true;
        }
        if (TextUtils.isEmpty(imagen)) {
            mensaje = mensaje + "\nEs necesario introducir una imagen del socio";
            error = true;
        }
        if (TextUtils.isEmpty(direccionProvincia)) {
            mensaje = mensaje + "\nEs necesario introducir una provincia del socio";
            error = true;
        }
        if (TextUtils.isEmpty(fecha)) {
            mensaje = mensaje + "\nEs necesario seleccionar una fecha";
            error = true;
        }
        if (TextUtils.isEmpty(String.valueOf(id))) {
            mensaje = mensaje + "\nEs necesario seleccionar un id";
            error = true;
        }
        if (!error) {
            return "";
        }
        else{
            return mensaje;
        }
    }
}
