package com.example.examenpmdm9062021.repo;

import android.content.Context;

import com.example.examenpmdm9062021.db.AppDatabase;
import com.example.examenpmdm9062021.db.model.Socio;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Repository {
    private AppDatabase appDatabase;

    public Repository(Context context){
        appDatabase = AppDatabase.getDBinstance(context);
    }

    public List<Socio> getAllSocioList(){
        List<Socio> list = new ArrayList<>();
        Future<List<Socio>> future = AppDatabase.executorService.submit(()->appDatabase.socioDao().getAllSocios());
        try {
            list = future.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void insert(Socio socio){
        AppDatabase.executorService.execute(()->appDatabase.socioDao().insertSocio(socio));
    }

    public void update(Socio socio){
        AppDatabase.executorService.execute(()->appDatabase.socioDao().updateSelectedSocio(socio.getNombreCompleto(), socio.getDireccionCalle(), socio.getDireccionProvincia(), socio.getFecha(), socio.getImagen(), socio.getId()));

    }

    public void delete(Socio socio){
        AppDatabase.executorService.execute(()->appDatabase.socioDao().deleteSocio(socio));
    }
}

