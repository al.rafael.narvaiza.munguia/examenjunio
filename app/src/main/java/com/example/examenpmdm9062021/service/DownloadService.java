package com.example.examenpmdm9062021.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;
import com.example.examenpmdm9062021.activities.MainActivity;
import org.jetbrains.annotations.NotNull;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class DownloadService extends Service {

    private final String urlString = "https://dam.org.es/files/socios.txt";

    public DownloadService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        showMessage("Creando el servicio");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        URL url = null;
        try {
            url = new URL(urlString);
            descargaOkHTTP(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            showMessage("Error en la URL: " + urlString);
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        showMessage("Servicio destruido");
    }

    @Override
    public IBinder onBind(Intent intent) {

        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void descargaOkHTTP(URL web) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(web).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (response.isSuccessful()) {
                        String myResponse = response.body().string();
                        enviarRespuesta(myResponse);
                    }
                }
            }
        });
    }

    private void enviarRespuesta (String mensaje) {
        Intent i = new Intent();
        i.setAction(MainActivity.ACTION_RESP);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.putExtra("resultado", mensaje);
        sendBroadcast(i);
    }

    private void showMessage(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}
