package com.example.examenpmdm9062021.recycler;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.examenpmdm9062021.activities.EditSocioActivity;
import com.example.examenpmdm9062021.databinding.RecyclerviewCardBinding;
import com.example.examenpmdm9062021.db.model.Socio;

import java.util.List;

public class SocioListAdapter extends RecyclerView.Adapter<SocioListAdapter.MyViewHolder> {


    private Context context;
    private List<Socio> socioList;
    private HandlePetClick clickListener;

    public SocioListAdapter(Context context, HandlePetClick clickListener) {
        this.context = context;
        this.clickListener = clickListener;
    }

    public void setSocioList(List<Socio> socioList) {
        this.socioList = socioList;
        notifyDataSetChanged();
    }


    @Override
    public SocioListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(RecyclerviewCardBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull SocioListAdapter.MyViewHolder holder, int position) {
        Socio socio = socioList.get(position);
        holder.binding.textViewSocioName.setText(socio.getNombreCompleto());
        holder.binding.textViewSocioDireccionCalle.setText(socio.getDireccionCalle());
        holder.binding.textViewSocioDireccionProvincia.setText(socio.getDireccionProvincia());
        holder.binding.textViewFecha.setText(String.valueOf(socio.getFecha()));
        Glide
                .with(context)
                .load(socio.getImagen())
                .into(holder.binding.imageViewSocio);
        holder.binding.imageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.deleteSocio(socioList.get(position));
            }
        });
        holder.binding.imageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clickListener.editSocio(socioList.get(position));
                Intent intent = new Intent(context, EditSocioActivity.class);
                intent.putExtra("id", socio.getId());
                intent.putExtra("nombre", socio.getNombreCompleto());
                intent.putExtra("direccionCalle", socio.getDireccionCalle());
                intent.putExtra("imagen", socio.getImagen());
                intent.putExtra("direccionProvincia", socio.getDireccionProvincia());
                intent.putExtra("fecha", socio.getFecha());
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        if (socioList == null || socioList.size() == 0) {
            return 0;
        } else
            return socioList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private RecyclerviewCardBinding binding;

        public MyViewHolder(@NonNull RecyclerviewCardBinding rvBinding) {
            super(rvBinding.getRoot());
            binding = rvBinding;
        }
    }

    public interface HandlePetClick {
        void editSocio(Socio socio);

        void deleteSocio(Socio socio);
    }
}
